#ifndef _POKER_ANAL_
#define _POKER_ANAL_

#include "poker_hands.h"

enum ePokerHandType
{
	high_card = 0,
	pair,
	pair_of_pairs,
	three_of_a_kind,
	straight,
	flush,
	full_house,
	four_of_a_kind,
	straight_flush
};

struct PokerHandScore
{
	int hand_type;
	char card1;
	char card2;
	char card3;
	char card4;
	char card5;
};

struct PokerStatsNum
{
	int num_equal_groups;
	struct _PokerGroup* equal_groups;
	
	int num_connecting;
	struct _PokerGroup* connecting_groups;
};

struct PokerStatsSuit
{
	int num_suited;
	struct _PokerFlushGroup* suited_groups;
};

struct PokerStatsConnSuited
{
	int num_connecting_suited;
	struct _PokerGroup* connecting_suited_groups;
};

struct PokerScoreInfo
{
	int num_fours;
	int num_threes;
	int num_twos;
	char best_4;
	char best_3;
	char second_best_3;
	char best_2;
	char second_best_2;
};



/* Given a _sorted_ hand of >=5 cards, find the best 5-card hand,
   and return its score using the following score system:

   Hand score: 0-8 for high card to straight flush
   Card scores: card1, card2 - each one is more important than the next
                cardX = 0(low ace), 1, 2, ..., 14 (high ace)
*/
void poker_score_hand( struct PokerHand* hHand,
	struct PokerHandScore* ret_score );

int poker_compare_scores( struct PokerHandScore* score1,
	struct PokerHandScore* score2 );

/* ------------ Less simple functions -------------- */

/* Do the first part of scoring hHand - put its hand_type into ret_score,
   and also return the function to call to find out the full score
   function_complete_score, and an argument to give to that function. */
void poker_score_hand_first_part(
	struct PokerHand* hHand,
	struct PokerHandScore* ret_score,
	void (**function_complete_score)( struct PokerHandScore*, struct PokerStatsNum*, struct PokerStatsNum*, struct PokerStatsSuit*, struct PokerSplitHand*, struct PokerSplitHand*, struct PokerSplitHand*, struct PokerStatsConnSuited*, struct PokerScoreInfo* ),
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info );

void poker_free_stats_num( struct PokerStatsNum* nStats );
void poker_free_stats_suit( struct PokerStatsSuit* sStats );
void poker_free_stats_conn_suited( struct PokerStatsConnSuited* nStats );

#endif
