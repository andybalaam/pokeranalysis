#ifndef _POKER_HANDS_
#define _POKER_HANDS_

typedef char PokerCard;

struct PokerHand
{
	int num_cards;
	PokerCard* cards;
};

struct PokerSplitCard
{
	char number;	// 0=Ace, 1=2, ..., 11=Queen 12=King
	char suit;	  // 0=Spades, 1=Clubs, 2=Diamonds, 3=Hearts
};

struct PokerSplitHand
{
	int num_cards;
	struct PokerSplitCard* split_cards;
};

/* Fill this hand with As, 2s, 3s, etc... */
void poker_get_first_hand( struct PokerHand* hand );

/* Find the next hand after the one supplied (in the arbirary order I am
   defining). (Note: always produces sorted hands.) */
void poker_get_next_hand( struct PokerHand* hand );

/* Find the next hand after the one supplied (in the arbirary order I am
   defining). (Note: always produces sorted hands.)  This version
   uses a smaller pack of the size you supply */
void poker_get_next_hand_smallpack( struct PokerHand* hand, char packsize );

/* Combine hand1 with hand2, where hand1 is chosen from a small
   pack not including the cards in hand2.  Adjust hand1 to
   reflect the missing cards in its pack, and combine it with
   hand2, and put the result into ret_combined_hand. */
/*void poker_combine_hands( struct PokerHand* ret_combined_hand,
	struct PokerHand* hand1, struct PokerHand* hand2 );*/

/* Combine hand1_unlimit with hand2, putting the result into
   ret_combined_hand.  If the inputs are sorted, the output is
   sorted too. */
void poker_combine_hands( struct PokerHand* ret_combined_hand,
    struct PokerHand* hand1, struct PokerHand* hand2 );

/* Given hand_limit that was created from a pack of size 52 minus
   the size of disallowed_hand, fill ret_hand with the cards of
   hand_limit but shifted so they don't overlap with any cards in
   disallowed hand.  This allows us to generate hands that come from
   the same pack as disallowed_hand in order using just
   get_next hand_smallpack to generate hand_limit and then
   poker_ulimit_hand to get the real cards.  NOTE: disallowed_hand
   must be sorted. */
void poker_unlimit_hand( struct PokerHand* ret_hand,
	struct PokerHand* hand_limit, struct PokerHand* disallowed_hand );

/*
Notation for hands of different encodings (all are of type
PokerHand, but represent different hands).

PokerHand* hHand;	// sorted-by-number, aces-high
PokerHand* lHand;	// sorted-by-number, aces-low
PokerHand* sHand;	// sorted-by-suit, aces-high
// sorted-by-suit, aces-low is never used
*/

/* Convert the supplied hand from sorted-by-suit encoding into
   sorted-by-number encoding.

number		: 00 01 02 ...		   50 51
sorted-by-suit: As 2s 3s ... Ks Ac ... Qh Kh
sorted-by-num : As Ac Ad Ah 2s ...  Kc Kd Kh

This leaves the aces high or low as they were before, so it can
also do this:

number		: 00 01 02 ...		   50 51
sorted-by-suit: 2s 3s ... Ks As 2c ... Kh Ah
sorted-by-num : 2s 2c 2d 2h 3s ...  Ac Ad Ah

*/
void poker_convert_hand_suit2num( struct PokerHand* hand );

/* Convert from sorted-by-number encoding to sorted-by-suit.  Always leaves
   hand sorted. */
void poker_convert_hand_num2suit( struct PokerHand* hand );

/*
Convert a hand in aces-low sorted-by-number encoding to an
aces-high sorted-by-number encoding:

number		: 00 01 02 ...		   50 51
aces-low	  : As Ac Ad Ah 2s ...  Kc Kd Kh
aces-high	 : 2s 2c 2d 2h 3s ...  Ac Ad Ah
*/
void poker_convert_numhand_aceslow2high( struct PokerHand* hand );

/* Convert a sorted-by-number-encoding hand from aces-high to aces-low
   Note that if the hand is sorted when it arrives, it will leave
   still sorted in its new encoding. */
void poker_convert_numhand_aceshigh2low( struct PokerHand* hand );

/* Make the cards in hand be sorted into numerical order i.e. the
   sort order will be different depending on their encoding. */
void poker_sort_hand( struct PokerHand* hand );

void poker_split_hand_num( const struct PokerHand* normal_hand,
	struct PokerSplitHand* ret_split_hand );

void poker_split_hand_suit( const struct PokerHand* normal_hand,
	struct PokerSplitHand* ret_split_hand );

void poker_unsplit_hand_suit( const struct PokerSplitHand* split_hand,
	struct PokerHand* ret_normal_hand );

void poker_copy_hand( struct PokerHand* to_hand, struct PokerHand* from_hand );

void poker_free_hand( struct PokerHand* hand );
void poker_free_split_hand( struct PokerSplitHand* nHand );

#endif
