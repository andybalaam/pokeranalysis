#include "poker_display.h"

#include <stdlib.h>

#define STATUS_FILENAME "status.txt"

#define NUM_SUITS 4
#define NUM_NUMS 13
// Note NUM_NUMS excludes the high ace.

char suits[] = { 's', 'c', 'd', 'h' };
char* cards[] = { "A", "2",  "3", "4", "5", "6", "7", "8",
				  "9", "10", "J", "Q", "K", "A" };

void _poker_print_hand( const struct PokerHand* hand,
	void (*function)( const PokerCard ) );
void _poker_fprint_hand( FILE* output_file, const struct PokerHand* hand,
	void (*function)( FILE* output_file, const PokerCard ) );
void _poker_print_card_num_acehigh( const PokerCard card );
void _poker_print_card_num_acelow( const PokerCard card );
void _poker_print_card_suit( const PokerCard card );
void _poker_print_card_code( const PokerCard card );
void _poker_fprint_card_code( FILE* output_file, const PokerCard card );
	
void poker_print_hand_suit( const struct PokerHand* hand )
{
	_poker_print_hand( hand, (_poker_print_card_suit) );
}

void poker_print_hand_num_acehigh( const struct PokerHand* hHand )
{
	_poker_print_hand( hHand, (_poker_print_card_num_acehigh) );
}

void poker_print_hand_num_acelow( const struct PokerHand* lHand )
{
	_poker_print_hand( lHand, (_poker_print_card_num_acelow) );
}

void poker_print_hand_code( const struct PokerHand* hand )
{
	_poker_print_hand( hand, (_poker_print_card_code) );
}

void poker_fprint_hand_code( FILE* output_file, const struct PokerHand* hand )
{
	_poker_fprint_hand( output_file, hand, (_poker_fprint_card_code) );
}

void poker_print_score( const struct PokerHandScore* score )
{
	switch( score->hand_type )
	{
		case high_card:
		{
			printf( "High cards: %s %s %s %s %s",
				cards[(int)score->card1], cards[(int)score->card2], 
				cards[(int)score->card3], cards[(int)score->card4],
				cards[(int)score->card5] );
			break;
		}
		case pair:
		{
			printf( "Pair of %ss followed by: %s %s %s",
				cards[(int)score->card1], cards[(int)score->card2], 
				cards[(int)score->card3], cards[(int)score->card4] );
			break;
		}
		case pair_of_pairs:
		{
			printf( "Pair of pairs: %ss and %ss followed by: %s",
				cards[(int)score->card1], cards[(int)score->card2], 
				cards[(int)score->card3] );
			break;
		}
		case three_of_a_kind:
		{
			printf( "Three %ss followed by: %s %s",
				cards[(int)score->card1], cards[(int)score->card2], 
				cards[(int)score->card3] );
			break;
		}
		case straight:
		{
			printf( "Straight %s high", cards[(int)score->card1] );
			break;
		}
		case flush:
		{
			printf( "Flush: %s %s %s %s %s",
				cards[(int)score->card1], cards[(int)score->card2], 
				cards[(int)score->card3], cards[(int)score->card4],
				cards[(int)score->card5] );
			break;
		}
		case full_house:
		{
			printf( "Full house: %ss over %ss",
				cards[(int)score->card1], cards[(int)score->card2] );
			break;
		}
		case four_of_a_kind:
		{
			printf( "Four %ss followed by: %s",
				cards[(int)score->card1], cards[(int)score->card2] );
			break;
		}
		case straight_flush:
		{
			printf( "Straight flush %s high",
				cards[(int)score->card1] );
			break;
		}
	}
}

void _poker_print_hand( const struct PokerHand* hand,
	void (*function)( const PokerCard ) )
{
	PokerCard* card = hand->cards;
	PokerCard* end_of_loop = card + hand->num_cards;
	for( ; card < end_of_loop; ++card )
	{
		(function)( *card );
	}
}

void _poker_fprint_hand( FILE* output_file, const struct PokerHand* hand,
	void (*function)( FILE* output_file, const PokerCard ) )
{
	PokerCard* card = hand->cards;
	PokerCard* end_of_loop = card + hand->num_cards;
	for( ; card < end_of_loop; ++card )
	{
		(function)( output_file, *card );
	}
}

void _poker_print_card_suit( const PokerCard card )
{
	printf( "%s%c ", cards[1+card%13], suits[card/13] );
}

void _poker_print_card_num_acehigh( const PokerCard card )
{
	PokerCard adjcard = card + 4;
	printf( "%s%c ", cards[adjcard/4], suits[adjcard%4] );
}

void _poker_print_card_num_acelow( const PokerCard card )
{
	printf( "%s%c ", cards[card/4], suits[card%4] );
}

void _poker_print_card_code( const PokerCard card )
{
	printf( "%d ", card );
}

void _poker_fprint_card_code( FILE* output_file, const PokerCard card )
{
	fprintf( output_file, "%d ", card );
}

void poker_input_hand_suit( char* string, struct PokerHand* ret_sHand )
{
	char* ch;
	int mode;	/* 0=number, 1=suit, 2=separator */
	struct PokerSplitHand split_sHand;
	split_sHand.num_cards = 0;
	split_sHand.split_cards = NULL;
	
	mode = 0;
	char num;
	char suit;
	num = 0;
	suit = 0;
	for( ch = string; *ch != 0; )
	{
		switch( mode )
		{
			case 0:
			{
				for( num = 0; num < NUM_NUMS; ++num )
				{
					int matches;
					char* nsc;
					char* tmp_ch;
					matches = 1;
					for( nsc = cards[(int)num+1], tmp_ch = ch; *nsc != 0; ++nsc, ++tmp_ch )
					{
						if( *nsc != *tmp_ch )
						{
							matches = 0;
						}
					}
					if( matches )
					{
						ch = tmp_ch;
						++mode;
						break;
					}
				}
				if( mode == 0 )
				{
					printf( "Failed to parse number '%s'.\n", ch );
					goto PARSING_FAILED;
				}
				break;
			}
			case 1:
			{
				for( suit = 0; suit < NUM_SUITS; ++suit )
				{
					if( suits[(int)suit] == *ch )
					{
						++ch;
						++mode;
						++split_sHand.num_cards;
						split_sHand.split_cards = realloc( split_sHand.split_cards,
							split_sHand.num_cards * sizeof( struct PokerSplitCard ) );
						struct PokerSplitCard* split_card =
							split_sHand.split_cards + split_sHand.num_cards - 1;
						split_card->suit = suit;
						split_card->number = num;
						break;
					}
				}
				if( mode == 1 )
				{
					printf( "Failed to parse suit '%c'.\n", *ch );
					goto PARSING_FAILED;
				}
				break;
			}
			case 2:
			{
				if( *ch == ' ' || *ch == '.' || *ch == '\n' )
				{
					++ch;
					mode = 0;
				}
				else
				{
					printf( "Expected ' ', '.' or '\n' but found '%c'.\n", *ch );
					goto PARSING_FAILED;
				}
				break;
			}
		}
	}
PARSING_FAILED:
	poker_unsplit_hand_suit( &split_sHand, ret_sHand );
	poker_free_split_hand( &split_sHand );
}

void poker_input_hand_code( char* string, struct PokerHand* ret_hand )
{
	char* ch;
	char* begin_num;
	
	ret_hand->num_cards = 0;
	ret_hand->cards = NULL;
		
	begin_num = string;
	for( ch = string; *ch != 0; ++ch )
	{
		if( *ch == ' ' || *ch == '\n' )
		{
			if( begin_num < ch )
			{
				++(ret_hand->num_cards);
				ret_hand->cards = realloc( ret_hand->cards,
					ret_hand->num_cards * sizeof( PokerCard ) );
				ret_hand->cards[ ret_hand->num_cards - 1 ] = atoi( begin_num );
			}
			begin_num = ch + 1;
		}
	}
	if( begin_num < ch )
	{
		++(ret_hand->num_cards);
		ret_hand->cards = realloc( ret_hand->cards,
			ret_hand->num_cards * sizeof( PokerCard ) );
		ret_hand->cards[ ret_hand->num_cards - 1 ] = atoi( begin_num );
	}
}

void poker_print_status( struct PokerHand* hCommunityHand,
    int* winnns, int* splits, int* played, int NUM_SCORES )
{
	int* last_winnns;
	int* win;
	int* spl;
	int* pla;
	FILE* output_file;
	
	output_file = fopen( STATUS_FILENAME, "w" );
	
	if( output_file == NULL )
	{
		fprintf( stderr, "Cannot open %s\n", STATUS_FILENAME );
		exit( 17 );
	}
	
	poker_fprint_hand_code( output_file, hCommunityHand );
	fprintf( output_file, "\n" );
	
	last_winnns = winnns + NUM_SCORES;
	for( win = winnns, spl = splits, pla = played;
		 win < last_winnns;
		 ++win, ++spl, ++pla )
	{
		fprintf( output_file, "%d %d %d\n", *win, *spl, *pla );
	}
	
	fclose( output_file );
}

int poker_load_status( struct PokerHand* hCommunityHand,
    int* winnns, int* splits, int* played, int NUM_SCORES )
{
	int* last_winnns;
	int* win;
	int* spl;
	int* pla;
	FILE* input_file;
	char buffy[1000];
	
	input_file = fopen( STATUS_FILENAME, "r" );
	
	if( !input_file )
	{
		fprintf( stderr, "No status file found.\n" );
		return 0;
	}
	
	fgets( buffy, 1000, input_file );
	poker_input_hand_code( buffy, hCommunityHand );
	
	last_winnns = winnns + NUM_SCORES;
	for( win = winnns, spl = splits, pla = played;
		 win < last_winnns;
		 ++win, ++spl, ++pla )
	{
		fscanf( input_file, "%d %d %d\n", win, spl, pla );
		/*fprintf( stderr, "%d %d %d\n", *win, *spl, *pla );*/
	}
	
	printf( "Status file read.  Current hand:\n" );
	poker_print_hand_num_acehigh( hCommunityHand );
	printf( "\n" );
	fflush( stdout );
	
	fclose( input_file );
	
	return 1;
}


