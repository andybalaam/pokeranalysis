#include "poker_hands.h"

#include <stdlib.h>
#include <string.h>

void poker_get_first_hand( struct PokerHand* hand )
{
	/* Assumptions:
		 hand.cards	 == NULL
		 hand.num_cards  < 52
	*/
	int i;
	
	hand->cards = malloc( hand->num_cards /* * sizeof( PokerCard )*/  );
	
	for( i = 0; i < hand->num_cards; ++i )
	{
		hand->cards[i] = i;
	}
}

void poker_get_next_hand( struct PokerHand* hand )
{
	poker_get_next_hand_smallpack( hand, 52 );
}

void poker_get_next_hand_smallpack( struct PokerHand* hand, char packsize )
{ 
	PokerCard counter_card;
	PokerCard* first_card = hand->cards;
	PokerCard* last_card = hand->cards + hand->num_cards - 1;
	PokerCard* card = last_card;
	
	++(*card);
	counter_card = packsize - 1;
	while( *card > counter_card && card >= first_card )
	{
		if( card > first_card )
		{
			--card;
			++(*card);
		}
		--counter_card;
	}
	
	counter_card = *card + 1;
	++card;
	while( card <= last_card )
	{
	   *card = counter_card;
	   ++card;
	   ++counter_card;
	}

	if( *last_card > packsize - 1 )
	{
		poker_free_hand( hand );
		hand->cards = NULL;
	}
}

void poker_combine_hands( struct PokerHand* ret_combined_hand,
	struct PokerHand* hand1, struct PokerHand* hand2 )
{
	PokerCard* card;
	PokerCard* last_card;
	PokerCard* card1;
	PokerCard* last_card1;
	PokerCard* card2;
	PokerCard* last_card2;
	
	ret_combined_hand->num_cards = hand1->num_cards + hand2->num_cards;
	ret_combined_hand->cards = malloc(
		ret_combined_hand->num_cards /* *sizeof( PokerCard )*/ );
	
	last_card = ret_combined_hand->cards + ret_combined_hand->num_cards - 1;
	last_card1 = hand1->cards + hand1->num_cards - 1;
	last_card2 = hand2->cards + hand2->num_cards - 1;
	
	for( card = ret_combined_hand->cards,
		 card1 = hand1->cards,
		 card2 = hand2->cards;
		 card <= last_card;
		 ++card )
	{
		if( card2 <= last_card2 && ( card1 > last_card1 || *card2 < *card1 ) )
		{
			*card = *card2;
			++card2;
		}
		else
		{
			*card = *card1;
			++card1;
		}
	}
}

void poker_unlimit_hand( struct PokerHand* ret_hand,
	struct PokerHand* hand_limit, struct PokerHand* disallowed_hand )
{
	PokerCard* card;
	PokerCard* last_card;
	PokerCard* ret_card;
	PokerCard* dis_card;
	PokerCard* last_dis_card;
	char card_value;
	
	ret_hand->num_cards = hand_limit->num_cards;
	ret_hand->cards = malloc( hand_limit->num_cards
		/* * sizeof( PokerCard )*/ );
	
	last_dis_card = disallowed_hand->cards + disallowed_hand->num_cards - 1;

	last_card = hand_limit->cards + hand_limit->num_cards - 1;
	for( card = hand_limit->cards, ret_card = ret_hand->cards;
		 card <= last_card;
		 ++card, ++ret_card )
	{
		card_value = *card;
		
		for( dis_card = disallowed_hand->cards;
			 dis_card <= last_dis_card;
			++dis_card )
		{
			if( *dis_card <= card_value )
			{
				++card_value;
			}
		}
		*ret_card = card_value;
	}
}

void poker_convert_hand_suit2num( struct PokerHand* hand )
{
/*
	FUTURE: there is a clever way here to do with using 4 suit markers
			going through each one and finding the lowest card across all
			the suits and putting it in next, but it might even be slower
			then the brite force method, and it's tricky, and this code
			isn't used in the "all possible hands" algorithm, so there
			doesn't seem much point over-optimising it.
			And, it seemed tricky.
			
	PokerCard* suit_markers[4];
	PokerCard* last_suit_marker;
	int suit_counter;
	PokerCard* card;
	PokerCard* last_card;
	
	last_card = hand->cards + hand->num_cards - 1;
	sm = suit_markers;
	for( card = hand->cards, card_counter = 0, suit_counter = 0;
		 card <= ;
		 ++card, ++card_counter )
	{
		if( suit_counter <= *card )
		{
			*sm = card;
			++sm;
			suit_counter += 13;
		}
	}
	
	last_suit_marker = suit_markers + 3;
	while( sm <= last_suit_marker )
	{
		*sm = NULL;
	}
	
	for( card = hand->cards, card_counter = 0, suit_counter = 0;
		 card <= ;
		 ++card, ++card_counter )
	*/
	
	PokerCard* card;
	PokerCard* last_card;
	last_card = hand->cards + hand->num_cards - 1;
	for( card = hand->cards; card <= last_card; ++card )
	{
		*card = (*card/13) + (*card%13)*4;
	}
	poker_sort_hand( hand );
}

void poker_convert_hand_num2suit( struct PokerHand* nHand )
{
	// This is faster than naive conversion followed by sorting,
	struct PokerHand suited_hands[4];
	struct PokerHand* s_hand;
	struct PokerHand* last_s_hand;
	PokerCard* card;
	PokerCard* last_card;
	PokerCard* s_card;
	PokerCard* last_s_card;
	
	last_s_hand = suited_hands + 3;
	for( s_hand = suited_hands; s_hand <= last_s_hand; ++s_hand )
	{
		s_hand->cards = malloc( nHand->num_cards /* * sizeof( PokerCard )*/ );
		s_hand->num_cards = 0;
	}
	
	last_card = nHand->cards + nHand->num_cards - 1;
	for( card = nHand->cards; card <= last_card; ++card )
	{
		char suit = *card%4;	// FUTURE: This is faster than a while loop,
								//		   but still a slow line.
		char suit_offset = suit*13;
		s_hand = suited_hands + suit;
		s_hand->cards[ s_hand->num_cards ] = suit_offset + (*card/4);
			// FUTURE: the above line is slow.  Can I speed it up?
		++s_hand->num_cards;
	}
	
	card = nHand->cards;
	for( s_hand = suited_hands; s_hand <= last_s_hand; ++s_hand )
	{
		last_s_card = s_hand->cards + s_hand->num_cards - 1;
		for( s_card = s_hand->cards;
			 s_card <= last_s_card;
			 ++s_card, ++card )
		{
			*card = *s_card;
		}
		poker_free_hand( s_hand );
	}
}


void poker_convert_numhand_aceslow2high( struct PokerHand* hand )
{
	PokerCard* card;
	for( card = hand->cards; card < hand->cards + hand->num_cards; ++card )
	{
		*card = ( *card + 48 ) % 52;
	}
}

void poker_convert_numhand_aceshigh2low( struct PokerHand* hand )
{
	// First adjust the cards so that the top 4 (aces) are moved to the bottom 4
	int needs_sorting = 0;
	PokerCard* card;
	PokerCard* last_card = hand->cards + hand->num_cards - 1;
	for( card = hand->cards; card <= last_card; ++card )
	{
		*card += 4;
		if( *card > 51 )
		{
			*card -=52;
			needs_sorting = 1;
		}
	}
	// Now, if we needed to move any aces, we need to adjust the order to leave
	// the hand properly sorted.
	if( needs_sorting )
	{
		// Remember the aces and others in separate arrays
		struct PokerHand aces;
		struct PokerHand others;
		PokerCard* card_counter;
		PokerCard* last_card_counter;
		aces.cards = malloc( 4 /* * sizeof( PokerCard )*/ );
		others.cards = malloc( hand->num_cards /* * sizeof( PokerCard ) */ );
		aces.num_cards = 0;
		others.num_cards = 0;
		for( card = hand->cards; card <= last_card; ++card )
		{
			if( *card < 4 )
			{
				aces.cards[aces.num_cards] = *card;
				++(aces.num_cards);
			}
			else
			{
				others.cards[others.num_cards] = *card;
				++(others.num_cards);
			}
		}
		// Copy the arrays, aces first, into the real hand.
		card = hand->cards;
		last_card_counter = aces.cards + aces.num_cards - 1;
		for( card_counter = aces.cards; card_counter <= last_card_counter; ++card_counter, ++card )
		{
			*card = *card_counter;
		}
		last_card_counter = others.cards + others.num_cards - 1;
		for( card_counter = others.cards; card_counter <= last_card_counter; ++card_counter, ++card )
		{
			*card = *card_counter;
		}
		poker_free_hand( &aces );
		poker_free_hand( &others );
	}
}

void poker_sort_hand( struct PokerHand* hand )
{
	/* FUTURE: Try quicksort here?  Hopefully we can normally avoid sorting
			   at all. */
	
	unsigned long long buff = 0;
	unsigned long long comp;
	unsigned long long end = 1LL << 52;
	char ch;
	PokerCard* card;
	PokerCard* last_card = hand->cards + hand->num_cards - 1;

	for( card = hand->cards; card <= last_card; ++card )
	{
		buff += 1LL << *card;
	}
	
	for( comp = 1LL, card = hand->cards, ch = 0;
		 card <= last_card && comp < end;
		 comp <<= 1, ++ch )
	{
		if( comp & buff )
		{
			*card = ch;
			++card;
		}
	}
}

void poker_split_hand_num( const struct PokerHand* normal_hand,
	struct PokerSplitHand* ret_split_hand )
{
	ret_split_hand->num_cards = normal_hand->num_cards;
	ret_split_hand->split_cards = malloc( ret_split_hand->num_cards * sizeof(struct PokerSplitCard)  );
	
	struct PokerSplitCard* split_card;
	PokerCard* normal_card;
	for( split_card = ret_split_hand->split_cards,
		 normal_card = normal_hand->cards;
		 split_card < ret_split_hand->split_cards + ret_split_hand->num_cards;
		 ++split_card, ++normal_card )
	{
		split_card->number = (*normal_card)/4;
		split_card->suit = (*normal_card)%4;
	}
}

void poker_split_hand_suit( const struct PokerHand* normal_hand,
	struct PokerSplitHand* ret_split_hand )
{
	struct PokerSplitCard* split_card;
	PokerCard* normal_card;
	PokerCard num;
	
	ret_split_hand->num_cards = normal_hand->num_cards;
	ret_split_hand->split_cards = malloc( ret_split_hand->num_cards * sizeof(struct PokerSplitCard)  );
	
	for( split_card = ret_split_hand->split_cards,
		 normal_card = normal_hand->cards;
		 split_card < ret_split_hand->split_cards + ret_split_hand->num_cards;
		 ++split_card, ++normal_card )
	{
		// The following is faster than % 13
		num = *normal_card;
		while( num > 12 )
		{
			num -= 13;
		}
		split_card->number = num;
		split_card->suit = (*normal_card)/13;	// FUTURE: slow line
	}
}

void poker_unsplit_hand_suit( const struct PokerSplitHand* split_hand,
    struct PokerHand* ret_normal_hand )
{
	ret_normal_hand->num_cards = split_hand->num_cards;
	ret_normal_hand->cards = malloc( ret_normal_hand->num_cards * sizeof(struct PokerSplitCard)  );
	
	struct PokerSplitCard* split_card;
	PokerCard* normal_card;
	for( normal_card = ret_normal_hand->cards,
		 split_card = split_hand->split_cards;
		 normal_card < ret_normal_hand->cards + ret_normal_hand->num_cards;
		 ++normal_card, ++split_card )
	{
		*normal_card = (split_card->suit*13) + split_card->number;
	}
}


void poker_copy_hand( struct PokerHand* to_hand, struct PokerHand* from_hand )
{
	int cards_size;
	
	to_hand->num_cards = from_hand->num_cards;
	
	cards_size = to_hand->num_cards /* * sizeof( PokerCard )*/;
	to_hand->cards = malloc( cards_size );
	memcpy( to_hand->cards, from_hand->cards, cards_size );
}

void poker_free_hand( struct PokerHand* hand )
{
	free( hand->cards );
}

void poker_free_split_hand( struct PokerSplitHand* split_hand )
{
	free( split_hand->split_cards );
}

