#ifndef _POKER_DISPLAY_
#define _POKER_DISPLAY_

#include "stdio.h"

#include "poker_hands.h"
#include "poker_anal.h"

/* Prints the hand using the sorted-by-suit-encoding, which is
   always encoded with aces high. */
void poker_print_hand_suit( const struct PokerHand* hand );
void poker_print_hand_num_acehigh( const struct PokerHand* hHand );
void poker_print_hand_num_acelow( const struct PokerHand* lHand );
void poker_print_hand_code( const struct PokerHand* hand );
void poker_fprint_hand_code( FILE* output_file, const struct PokerHand* hand );

void poker_print_score( const struct PokerHandScore* score );

/**
 * If string contains a list of cards like 8d.10h.Qs.4d (either . or
 * space can separate cards) then PokerHand will be allocated and
 * filled with the order-by-suit-encoded cards they represent.
 */
void poker_input_hand_suit( char* string, struct PokerHand* ret_sHand );
void poker_input_hand_code( char* string, struct PokerHand* ret_sHand );

void poker_print_status( struct PokerHand* hCommunityHand,
    int* winnns, int* splits, int* played, int NUM_SCORES );

int poker_load_status( struct PokerHand* hCommunityHand,
    int* winnns, int* splits, int* played, int NUM_SCORES );

#endif
