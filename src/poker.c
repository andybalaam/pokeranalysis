#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>

#include "poker_hands.h"
#include "poker_display.h"
#include "poker_anal.h"

int poker_convert_test( int argc, char** argv );
int poker_list_hands( int argc, char** argv );
int poker_test_sorting( int argc, char** argv );
int poker_sort_everything( int argc, char** argv );
void poker_get_num_cards( struct PokerHand* hand, int argc, char** argv );

int score_heads_up( int argc, char** argv );
int evaluate_all_heads_up( int argc, char** argv );
int score_everything( int argc, char** argv );
int score_everything_quick( int argc, char** argv );

#define NUM_SCORES 2652

/* TODO: can we combine analyse_numacehigh and analyse_numacelow into one? */

/* Only do the detail (i.e. not just "flush" but "flush ace high etc."
   if we have the same score as a score passed in to poker_score_hand. */

/* TODO:
	Choose the first card of the 5 from [0:12]
	Choose the next from [0:25]
	If the second card was > 12, flip the first bool
	If the first bool was flipped, choose the next card from [0:38]
	Otherwise, choose it from [0:25] again
	If this card was > 25, flip the second bool
	If the second bool was flipped, choose the next card from [0:51]
	Otherwise, choose it as above from [0:25] or [0:38]

	Additionally:
	Use these same bools when choosing the pairs of cards and the same system.
	But this time, we need to adjust the thresholds according to what is in
	the 5. */

/* FUTURE: would getting rid of all mallocs make it faster? */

/*int main (int argc, char **argv)
{
	int c;
	int digit_optind = 0;

	while( 1 )
	{
		int this_option_optind = optind ? optind : 1;
		int option_index = 0;
		
		static struct option long_options[] =
		{
			{"add", 1, 0, 0},
			{"append", 0, 0, 0},
			{"delete", 1, 0, 0},
			{"verbose", 0, 0, 0},
			{"create", 1, 0, ’c’},
			{"file", 1, 0, 0},
			{0, 0, 0, 0}
		};

		c = getopt_long (argc, argv, "abc:d:012",
				long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
		case 0:
			printf ("option %s", long_options[option_index].name);
			if (optarg)
				printf (" with arg %s", optarg);
			printf ("\n");
			break;

		case ’0’:
		case ’1’:
		case ’2’:
			if (digit_optind != 0 && digit_optind != this_option_optind)
			 printf ("digits occur in two different argv-elements.\n");
			digit_optind = this_option_optind;
			printf ("option %c\n", c);
			break;

		case ’a’:
			printf ("option a\n");
			break;

		case ’b’:
			printf ("option b\n");
			break;
	case ’c’:
			printf ("option c with value ‘%s’\n", optarg);
			break;

		case ’d’:
			printf ("option d with value ‘%s’\n", optarg);
			break;

		case ’?’:
			break;

		default:
			printf ("?? getopt returned character code 0%o ??\n", c);
		}
	}

	if (optind < argc) {
		printf ("non-option ARGV-elements: ");
		while (optind < argc)
			printf ("%s ", argv[optind++]);
		printf ("\n");
	}

	exit (0);
}*/


int main( int argc, char** argv )
{
	//return score_everything( argc, argv );
	//return score_everything_quick( argc, argv );
	return evaluate_all_heads_up( argc, argv );
	//return score_heads_up( argc, argv );
}

int score_heads_up( int argc, char** argv )
{
	struct PokerHand p1_hand;
	struct PokerHand p1_hand_full;
	struct PokerHand p2_hand;
	struct PokerHand p2_hand_full;
	struct PokerHand community_hand;
	struct PokerHandScore p1_score;
	struct PokerHandScore p2_score;

	if( argc < 4 )
	{
		printf( "Usage: ./poker hand1 hand2 community_hand\n" );
		return 1;
	}
	
	poker_input_hand_suit( argv[1], &p1_hand );
	poker_sort_hand( &p1_hand );
	poker_convert_hand_suit2num( &p1_hand );
	
	poker_input_hand_suit( argv[2], &p2_hand );
	poker_sort_hand( &p2_hand );
	poker_convert_hand_suit2num( &p2_hand );
	
	poker_input_hand_suit( argv[3], &community_hand );
	poker_sort_hand( &community_hand );
	poker_convert_hand_suit2num( &community_hand );
	
	poker_combine_hands( &p1_hand_full, &p1_hand, &community_hand );
	poker_combine_hands( &p2_hand_full, &p2_hand, &community_hand );
	
	poker_score_hand( &p1_hand_full, &p1_score );
	poker_score_hand( &p2_hand_full, &p2_score );
	
	printf( "\nPlayer 1: " );
	poker_print_score( &p1_score );
	
	printf( "\nPlayer 2: " );
	poker_print_score( &p2_score );
	printf( "\n" );
	
	int cmp = poker_compare_scores( &p1_score, &p2_score );
	if( cmp > 0 )
	{
		printf( "Player 1 wins.\n\n" );
	}
	else if( cmp < 0 )
	{
		printf( "Player 2 wins.\n\n" );
	}
	else
	{
		printf( "Split pot.\n\n" );
	}
	
	poker_free_hand( &p1_hand );
	poker_free_hand( &p1_hand_full );
	poker_free_hand( &p2_hand );
	poker_free_hand( &p2_hand_full );
	poker_free_hand( &community_hand );
	
	return 0;
}

int score_single_hand( int argc, char** argv )
{
	char string[512];
	struct PokerHand hand;
	struct PokerHandScore score;

	printf( "Enter the hand you want to analyse:\n" );
	fflush( stdout );

	fgets( string, 512, stdin );
	poker_input_hand_suit( string, &hand );	// Suit-encoded
	//poker_print_hand_suit( &hand );
	poker_convert_hand_suit2num( &hand );	// Now aces-high-number-encoded
	poker_sort_hand( &hand );
	//poker_print_hand_num_acehigh( &hand );
	poker_score_hand( &hand, &score );
	poker_print_score( &score );

	poker_free_hand( &hand );
	return 0;
}

int evaluate_all_heads_up( int argc, char** argv )
{
	struct PokerHand hCommunityHand;
	struct PokerHand hPlayer1Hand_limit, hPlayer1Hand_unlimit;
	struct PokerHand hPlayer1Hand_full;
	struct PokerHand hPlayer2Hand_limit, hPlayer2Hand_unlimit;
	struct PokerHand hPlayer2Hand_full;
	struct PokerHandScore player1Score;
	struct PokerHandScore player2Score;
	int winnns[NUM_SCORES];
	int splits[NUM_SCORES];
	int played[NUM_SCORES];
	int hand1_counter;
	int hand2_counter;
	int status_counter;
	
	struct PokerStatsNum hStats;
	struct PokerStatsNum lStats;
	struct PokerStatsSuit sStats;
	struct PokerSplitHand split_hHand;
	struct PokerSplitHand split_lHand;
	struct PokerSplitHand split_sHand;
	struct PokerStatsConnSuited hcsStats;
	struct PokerScoreInfo score_info;
	
	void (*function_complete_score)( struct PokerHandScore*, struct PokerStatsNum*, struct PokerStatsNum*, struct PokerStatsSuit*, struct PokerSplitHand*, struct PokerSplitHand*, struct PokerSplitHand*, struct PokerStatsConnSuited*, struct PokerScoreInfo* );
	
	memset( winnns, 0, NUM_SCORES * sizeof( int ) );
	memset( splits, 0, NUM_SCORES * sizeof( int ) );
	memset( played, 0, NUM_SCORES * sizeof( int ) );
	
	hCommunityHand.num_cards = 5;
	hPlayer1Hand_limit.num_cards = 2;
	hPlayer1Hand_unlimit.num_cards = 2;
	hPlayer2Hand_limit.num_cards = 2;
	hPlayer2Hand_unlimit.num_cards = 2;
	
	/* Either load status from disk... */
	if( !poker_load_status( &hCommunityHand, winnns, splits, played,
		NUM_SCORES ) )
	{
		/* ... or start from the beginning. */
		poker_get_first_hand( &hCommunityHand );
	}
	
	/* Step through all remaining possible of community cards. */
	for( status_counter = 0;
		 hCommunityHand.cards != NULL;
		 poker_get_next_hand( &hCommunityHand ), ++status_counter )
		 /*poker_get_next_hand_smallpack( &hCommunityHand, 7 ) )*/
	{
		if( status_counter == 100 )
		{
			status_counter = 0;
			poker_print_status( &hCommunityHand, winnns, splits, played,
				NUM_SCORES );
		}
		
		for( poker_get_first_hand( &hPlayer1Hand_limit );
			 hPlayer1Hand_limit.cards  != NULL;
			 poker_get_next_hand_smallpack( &hPlayer1Hand_limit, 47 ) )
		{
			poker_unlimit_hand( &hPlayer1Hand_unlimit,
				&hPlayer1Hand_limit, &hCommunityHand );
			
			poker_combine_hands( &hPlayer1Hand_full,
				&hPlayer1Hand_unlimit, &hCommunityHand );
			
			poker_score_hand( &hPlayer1Hand_full, &player1Score );
			hand1_counter = hPlayer1Hand_unlimit.cards[0] * 52 + hPlayer1Hand_unlimit.cards[1];
			
			/*
			poker_print_hand_num_acehigh( &hPlayer1Hand_full ); printf( "\n" );
			poker_print_score( &player1Score );                 printf( "\n" );
			*/
			
			for( poker_get_first_hand( &hPlayer2Hand_limit );
				 hPlayer2Hand_limit.cards != NULL;
				 poker_get_next_hand_smallpack( &hPlayer2Hand_limit, 45 ) )
			{
				int cmp;
				poker_unlimit_hand( &hPlayer2Hand_unlimit,
					&hPlayer2Hand_limit, &hPlayer1Hand_full );
			
				poker_combine_hands( &hPlayer2Hand_full,
					&hPlayer2Hand_unlimit, &hCommunityHand );

				hand2_counter = hPlayer2Hand_unlimit.cards[0] * 52 + hPlayer2Hand_unlimit.cards[1];
				++played[ hand1_counter ];
				++played[ hand2_counter ];
				
				memset( &hStats, 0, sizeof( struct PokerStatsNum ) );
				memset( &lStats, 0, sizeof( struct PokerStatsNum ) );
				memset( &sStats, 0, sizeof( struct PokerStatsSuit ) );
				memset( &split_hHand, 0, sizeof( struct PokerSplitHand ) );
				memset( &split_lHand, 0, sizeof( struct PokerSplitHand ) );
				memset( &split_sHand, 0, sizeof( struct PokerSplitHand ) );
				memset( &hcsStats, 0, sizeof( struct PokerStatsConnSuited ) );
				memset( &score_info, 0, sizeof( struct PokerScoreInfo ) );
		
				poker_score_hand_first_part( &hPlayer2Hand_full, &player2Score,
					&function_complete_score,
					&hStats, &lStats, &sStats,
					&split_hHand, &split_lHand, &split_sHand,
					&hcsStats, &score_info );
				
				if( player1Score.hand_type == player2Score.hand_type )
				{
					(*function_complete_score)(
						&player2Score,
						&hStats, &lStats, &sStats,
						&split_hHand, &split_lHand, &split_sHand,
						&hcsStats, &score_info );
				}
				
				cmp = poker_compare_scores( &player1Score, &player2Score );
				if( cmp > 0 )
				{
					++winnns[ hand1_counter ];
				}
				else if( cmp < 0 )
				{
					++winnns[ hand2_counter ];
				}
				else
				{
					++splits[ hand1_counter ];
					++splits[ hand2_counter ];
				}
				
				poker_free_stats_num( &hStats );
				poker_free_stats_num( &lStats );
				poker_free_stats_suit( &sStats );
				poker_free_split_hand( &split_hHand );
				poker_free_split_hand( &split_lHand );
				poker_free_split_hand( &split_sHand );
				poker_free_stats_conn_suited( &hcsStats );
				
				poker_free_hand( &hPlayer2Hand_unlimit );
				poker_free_hand( &hPlayer2Hand_full );
			}
			poker_free_hand( &hPlayer2Hand_limit );
			poker_free_hand( &hPlayer1Hand_unlimit );
			poker_free_hand( &hPlayer1Hand_full );
		}
		poker_free_hand( &hPlayer1Hand_limit );

		/*poker_score_hand( &hCommunityHand, &score );
		poker_print_score( &score );
		printf( "\n" );*/
	}

	{
		// Write out the results so far
		struct PokerHand hTempHand;
		int hand_counter;
		hTempHand.num_cards = 2;
		for( poker_get_first_hand( &hTempHand );
			 hTempHand.cards  != NULL;
		 	 poker_get_next_hand( &hTempHand ) )
		{
			hand_counter = hTempHand.cards[0] * 52 + hTempHand.cards[1];
			int pl = played[hand_counter];
			int wi = winnns[hand_counter];
			int sp = splits[hand_counter];
			
			poker_print_hand_num_acehigh( &hTempHand );
			printf( "played=%d wins=%d (%f%%) splits=%d (%f%%)\n", 
				pl, wi, 100*(double)wi/(double)pl, sp, 100*(double)sp/(double)pl );
		}
		poker_free_hand( &hTempHand );
	}
	
	poker_free_hand( &hCommunityHand );
	
	return 0;
}

int test_combine( int argc, char** argv )
{
	struct PokerHand hCommunityHand;
	struct PokerHand hPlayer1Hand_limit, hPlayer1Hand;
	struct PokerHand hPlayer2Hand_limit, hPlayer2Hand;
	
	hCommunityHand.num_cards = 5;
	hCommunityHand.cards = malloc( hCommunityHand.num_cards 
		/* * sizeof( PokerCard )*/ );
	
	hCommunityHand.cards[0] = 0;
	hCommunityHand.cards[1] = 1;
	hCommunityHand.cards[2] = 2;
	hCommunityHand.cards[3] = 3;
	hCommunityHand.cards[4] = 4;
	
	poker_print_hand_num_acehigh( &hCommunityHand );
	
	hPlayer1Hand_limit.num_cards = 2;
	hPlayer1Hand_limit.cards = malloc( hCommunityHand.num_cards
		/* * sizeof( PokerCard )*/ );
	
	hPlayer1Hand_limit.cards[0] = 0;
	hPlayer1Hand_limit.cards[1] = 1;
	poker_combine_hands( &hPlayer1Hand, &hPlayer1Hand_limit,
		&hCommunityHand );
	
	poker_sort_hand( &hPlayer1Hand );
	
	hPlayer2Hand_limit.num_cards = 2;
	hPlayer2Hand_limit.cards = malloc( hCommunityHand.num_cards
		/* * sizeof( PokerCard )*/ );
	
	hPlayer2Hand_limit.cards[0] = 0;
	hPlayer2Hand_limit.cards[1] = 1;
	poker_combine_hands( &hPlayer2Hand, &hPlayer2Hand_limit,
		&hCommunityHand );
	
	poker_print_hand_num_acehigh( &hPlayer1Hand );
	poker_print_hand_num_acehigh( &hPlayer2Hand );
	
	return 1;
}

int score_everything_quick( int argc, char** argv )
{
	struct PokerHand hHand;
	struct PokerHandScore score;
    
	void (*function_complete_score)( struct PokerHandScore*, struct PokerStatsNum*, struct PokerStatsNum*, struct PokerStatsSuit*, struct PokerSplitHand*, struct PokerSplitHand*, struct PokerSplitHand*, struct PokerStatsConnSuited*, struct PokerScoreInfo* );
	
	struct PokerStatsNum hStats;
	struct PokerStatsNum lStats;
	struct PokerStatsSuit sStats;
	struct PokerSplitHand split_hHand;
	struct PokerSplitHand split_lHand;
	struct PokerSplitHand split_sHand;
	struct PokerStatsConnSuited hcsStats;
	struct PokerScoreInfo score_info;
	
	int handtypes[9];	// We increment the relevant one of these every time
						// we find a hand of this type
	int total;
	
	memset( handtypes, 0, 9 * sizeof( int ) );
	
	poker_get_num_cards( &hHand, argc, argv );	
	poker_get_first_hand( &hHand );
	
	total = 0;
	while( hHand.cards != NULL )
	{
		memset( &hStats, 0, sizeof( struct PokerStatsNum ) );
		memset( &lStats, 0, sizeof( struct PokerStatsNum ) );
		memset( &sStats, 0, sizeof( struct PokerStatsSuit ) );
		memset( &split_hHand, 0, sizeof( struct PokerSplitHand ) );
		memset( &split_lHand, 0, sizeof( struct PokerSplitHand ) );
		memset( &split_sHand, 0, sizeof( struct PokerSplitHand ) );
		memset( &hcsStats, 0, sizeof( struct PokerStatsConnSuited ) );
		memset( &score_info, 0, sizeof( struct PokerScoreInfo ) );
		
		poker_score_hand_first_part( &hHand, &score, &function_complete_score,
			&hStats, &lStats, &sStats,
			&split_hHand, &split_lHand, &split_sHand,
			&hcsStats, &score_info );
		
		++handtypes[score.hand_type];
		++total;
		
		poker_free_stats_num( &hStats );
		poker_free_stats_num( &lStats );
		poker_free_stats_suit( &sStats );
		poker_free_split_hand( &split_hHand );
		poker_free_split_hand( &split_lHand );
		poker_free_split_hand( &split_sHand );
		poker_free_stats_conn_suited( &hcsStats );
		
		//poker_print_score( &score );
		//printf( "\n" );
		poker_get_next_hand( &hHand );
	}
	
	printf( "Straight flush:  %f%%\n",
		100 * ((double)handtypes[straight_flush]/(double)total) );
	
	printf( "Four of a kind:  %f%%\n",
		100 * ((double)handtypes[7]/(double)total) );
	
	printf( "Full house:      %f%%\n",
		100 * ((double)handtypes[6]/(double)total) );
	
	printf( "Flush:           %f%%\n",
		100 * ((double)handtypes[5]/(double)total) );
	
	printf( "Straight:        %f%%\n",
		100 * ((double)handtypes[4]/(double)total) );
	
	printf( "Three of a kind: %f%%\n",
		100 * ((double)handtypes[3]/(double)total) );
	
	printf( "Pair of pairs:   %f%%\n",
		100 * ((double)handtypes[2]/(double)total) );
	
	printf( "Pair:            %f%%\n",
		100 * ((double)handtypes[1]/(double)total) );
	
	printf( "High card:       %f%%\n",
		100 * ((double)handtypes[high_card]/total) );
	
	return 0;
}

int score_everything( int argc, char** argv )
{
	struct PokerHand hHand;
	struct PokerHandScore score;
	int handtypes[9];	// We increment the relevant one of these every time
						// we find a hand of this type
	int total;
	
	memset( handtypes, 0, 9 * sizeof( int ) );
	
	poker_get_num_cards( &hHand, argc, argv );	
	poker_get_first_hand( &hHand );
	
	total = 0;
	while( hHand.cards != NULL )
	{
		//poker_print_hand_num_acehigh( &hHand );
		//printf( " : " );
		poker_score_hand( &hHand, &score );
		
		++handtypes[score.hand_type];
		++total;
		
		//poker_print_score( &score );
		//printf( "\n" );
		poker_get_next_hand( &hHand );
	}
	
	printf( "Straight flush:  %f%%\n",
		100 * ((double)handtypes[straight_flush]/(double)total) );
	
	printf( "Four of a kind:  %f%%\n",
		100 * ((double)handtypes[7]/(double)total) );
	
	printf( "Full house:      %f%%\n",
		100 * ((double)handtypes[6]/(double)total) );
	
	printf( "Flush:           %f%%\n",
		100 * ((double)handtypes[5]/(double)total) );
	
	printf( "Straight:        %f%%\n",
		100 * ((double)handtypes[4]/(double)total) );
	
	printf( "Three of a kind: %f%%\n",
		100 * ((double)handtypes[3]/(double)total) );
	
	printf( "Pair of pairs:   %f%%\n",
		100 * ((double)handtypes[2]/(double)total) );
	
	printf( "Pair:            %f%%\n",
		100 * ((double)handtypes[1]/(double)total) );
	
	printf( "High card:       %f%%\n",
		100 * ((double)handtypes[high_card]/total) );
	
	return 0;
	
}


void poker_get_num_cards( struct PokerHand* hand, int argc, char** argv )
{
	if( argc > 1 )
	{
		hand->num_cards = atoi( argv[1] );
	}
	else
	{
		hand->num_cards = 5;
	}
	hand->cards = NULL;
}



int poker_sort_everything( int argc, char** argv )
{
	struct PokerHand hand;
	poker_get_num_cards( &hand, argc, argv );
	
	poker_get_first_hand( &hand );
	
	while( hand.cards != NULL )
	{
		poker_sort_hand( &hand );
		poker_get_next_hand( &hand );
	}

	return 0;
}

int poker_test_sorting( int argc, char** argv )
{
	struct PokerHand hand;
	hand.num_cards = 5;
	hand.cards = malloc( hand.num_cards /* * sizeof( PokerCard )*/ );
	
	hand.cards[0] = 23;
	hand.cards[1] = 1;
	hand.cards[2] = 17;
	hand.cards[3] = 50;
	hand.cards[4] = 49;
	
	poker_print_hand_num_acehigh( &hand );
	
	poker_sort_hand( &hand );
	
	poker_print_hand_num_acehigh( &hand );
	
	poker_convert_hand_num2suit( &hand );

	poker_sort_hand( &hand );

	poker_print_hand_suit( &hand );
	
	return 0;
}

int poker_convert_test( int argc, char** argv )
{
	int i;
	struct PokerHand hand;
	poker_get_num_cards( &hand, argc, argv );

	poker_get_first_hand( &hand );
	
	for( i=0; i<24; ++i )
	{
		poker_get_next_hand( &hand );
	}
	
	poker_print_hand_num_acehigh( &hand );
	
	poker_convert_hand_num2suit( &hand );

	poker_print_hand_suit( &hand );
	
	poker_convert_hand_suit2num( &hand );
	
	poker_print_hand_num_acehigh( &hand );
	
	return 0;
}

int poker_list_hands( int argc, char** argv )
{
	struct PokerHand hand;
	if( argc > 1 )
	{
		hand.num_cards = atoi( argv[1] );
	}
	else
	{
		hand.num_cards = 5;
	}
	hand.cards = NULL;
	poker_get_first_hand( &hand );
	
	while( hand.cards != NULL )
	{
		poker_print_hand_num_acehigh( &hand );
		poker_get_next_hand( &hand );
	}

	return 0;
}
