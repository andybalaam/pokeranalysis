#include "poker_anal.h"

#include <stdlib.h>
#include <string.h>

#define FALSE 0
#define TRUE 1

struct _PokerGroup
{
	int group_size;	 // How many cards are in this group
	char number;		// Card number (0=A, 1=2, ...) - either the value of the
						// equal cards, or the top card of a straight, etc.
};

struct _PokerFlushGroup
{
	int group_size;
	int suit;
	char numbers[5];
};
/*
struct PokerScoreInfo_four_of_a_kind
{
	char best_4;
	struct PokerSplitHand* split_hHand;
};

struct PokerScoreInfo_full_house
{	
	char best_3;
	char second_best_3;
	char best_2;
};

struct PokerScoreInfo_three_of_a_kind
{	
	char best_3;
	struct PokerSplitHand* split_hHand;
};

struct PokerScoreInfo_pair_of_pairs
{	
	char best_2;
	char second_best_2;
	struct PokerSplitHand* split_hHand;
};

struct PokerScoreInfo_pair
{	
	char best_2;
	struct PokerSplitHand* split_hHand;
};
*/

void _poker_analyse_numacehigh( struct PokerSplitHand* split_hHand,
	struct PokerStatsNum* hStats );

void _poker_analyse_numacelow( struct PokerSplitHand* split_lHand,
	struct PokerStatsNum* lStats );

void _poker_analyse_connecting_suited( struct PokerSplitHand* split_nHand,
	struct PokerStatsConnSuited* cStats );

void _poker_analyse_suit( struct PokerSplitHand* split_sHand,
	struct PokerStatsSuit* sStats );

void _poker_finished_connecting_suited_group(
	struct PokerStatsConnSuited* nStats,
	const int cur_connecting_suited_count,
	const char cur_connecting_suited_number );

void _poker_finished_connecting_group( struct PokerStatsNum* nStats,
	const int cur_connecting_count,
	const char cur_connecting_number );

void _poker_finished_equal_group( struct PokerStatsNum* nStats,
	const int cur_equal_count,
	const char cur_equal_number );

void _poker_finished_suited_group( struct PokerStatsSuit* sStats,
	struct _PokerFlushGroup* cur_group );

char _poker_find_best_group_num( struct _PokerGroup* grp, int size );

void _poker_find_best_group_suit( struct _PokerFlushGroup* grp, int size,
	struct _PokerFlushGroup** ret_best_group );

char _poker_find_next_best_card( struct PokerSplitHand* split_hHand,
	char* values_to_ignore, int num_ignores );

void poker_complete_score_staight_flush(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info );
	
void poker_complete_score_four_of_a_kind(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info );

void poker_complete_score_full_house(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info );

void poker_complete_score_flush(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info );

void poker_complete_score_straight_ace_high(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info );

void poker_complete_score_straight_ace_low(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info );

void poker_complete_score_three_of_a_kind(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info );

void poker_complete_score_pair_of_pairs(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info );

void poker_complete_score_pair(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info );

void poker_complete_score_high_card(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info );

/* --------------------- */

void poker_score_hand( struct PokerHand* hHand,
	struct PokerHandScore* ret_score )
{
	void (*function_complete_score)( struct PokerHandScore*, struct PokerStatsNum*, struct PokerStatsNum*, struct PokerStatsSuit*, struct PokerSplitHand*, struct PokerSplitHand*, struct PokerSplitHand*, struct PokerStatsConnSuited*, struct PokerScoreInfo* );
	
	struct PokerStatsNum hStats;
	struct PokerStatsNum lStats;
	struct PokerStatsSuit sStats;
	struct PokerSplitHand split_hHand;
	struct PokerSplitHand split_lHand;
	struct PokerSplitHand split_sHand;
	struct PokerStatsConnSuited hcsStats;
	struct PokerScoreInfo score_info;
	
	memset( &hStats, 0, sizeof( struct PokerStatsNum ) );
	memset( &lStats, 0, sizeof( struct PokerStatsNum ) );
	memset( &sStats, 0, sizeof( struct PokerStatsSuit ) );
	memset( &split_hHand, 0, sizeof( struct PokerSplitHand ) );
	memset( &split_lHand, 0, sizeof( struct PokerSplitHand ) );
	memset( &split_sHand, 0, sizeof( struct PokerSplitHand ) );
	memset( &hcsStats, 0, sizeof( struct PokerStatsConnSuited ) );
	memset( &score_info, 0, sizeof( struct PokerScoreInfo ) );
	
	poker_score_hand_first_part( hHand, ret_score, &function_complete_score,
		&hStats, &lStats, &sStats,
		&split_hHand, &split_lHand, &split_sHand,
		&hcsStats, &score_info );
	
	(*function_complete_score)(
		ret_score,
		&hStats, &lStats, &sStats,
		&split_hHand, &split_lHand, &split_sHand,
		&hcsStats, &score_info );
	
	poker_free_stats_num( &hStats );
	poker_free_stats_num( &lStats );
	poker_free_stats_suit( &sStats );
	poker_free_split_hand( &split_hHand );
	poker_free_split_hand( &split_lHand );
	poker_free_split_hand( &split_sHand );
	poker_free_stats_conn_suited( &hcsStats );
}

void poker_score_hand_first_part(
	struct PokerHand* hHand,
	struct PokerHandScore* ret_score,
	void (**function_complete_score)( struct PokerHandScore*, struct PokerStatsNum*, struct PokerStatsNum*, struct PokerStatsSuit*, struct PokerSplitHand*, struct PokerSplitHand*, struct PokerSplitHand*, struct PokerStatsConnSuited*, struct PokerScoreInfo* ),
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info )
{
	int done;
	
	int done_lStats;
	struct PokerHand lHand;
	
	int done_sStats;
	struct PokerHand sHand;
	
	done = FALSE;
	done_lStats = FALSE;
	done_sStats = FALSE;
	
	ret_score->hand_type = high_card;
	
	poker_split_hand_num( hHand, split_hHand );
	_poker_analyse_numacehigh( split_hHand, hStats );
	
	if( hStats->num_connecting > 0 )
	{   // There is a straight - do we have a straight flush?
		done_sStats = TRUE;
		poker_copy_hand( &sHand, hHand );
		poker_convert_hand_num2suit( &sHand );
		poker_split_hand_suit( &sHand, split_sHand );
		_poker_analyse_suit( split_sHand, sStats );
		
		if( sStats->num_suited > 0 )
		{	// Straight and flush - possible straight flush
			_poker_analyse_connecting_suited( split_hHand, hcsStats );
			if( hcsStats->num_connecting_suited > 0 )
			{	// We have a straight flush!  Find the best one(!!)
				ret_score->hand_type = straight_flush;
				*function_complete_score = &poker_complete_score_staight_flush;
				done = TRUE;
			}
			else
			{
				poker_free_stats_conn_suited( hcsStats );
			}
		}
	}
	
	// Only analyse aces-low if we have an ace in the hand.  If there is an
	// ace, the last card in hHand will be an ace.
	if( *(hHand->cards + hHand->num_cards - 1) > 47 )
	{
		done_lStats = TRUE;
		poker_copy_hand( &lHand, hHand );
		poker_convert_numhand_aceshigh2low( &lHand );
		poker_split_hand_num( &lHand, split_lHand );
		_poker_analyse_numacelow( split_lHand, lStats );
	}
	
	// If we didn't find a straight flush, look for other things
	if( !done )
	{
		if( done_lStats && lStats->num_connecting > 0 )
		{   // There is a straight (ace low) - do we have a straight flush?
			if( !done_sStats )
			{
				done_sStats = TRUE;
				poker_copy_hand( &sHand, hHand );
				poker_convert_hand_num2suit( &sHand );
				poker_split_hand_suit( &sHand, split_sHand );
				_poker_analyse_suit( split_sHand, sStats );
			}
			
			if( sStats->num_suited > 0 )
			{	// Straight and flush - possible straight flush
				_poker_analyse_connecting_suited( split_lHand, hcsStats );
				if( hcsStats->num_connecting_suited > 0 )
				{	// We have a straight flush!  Find the best one(!!)
					ret_score->hand_type = straight_flush;
					
					*function_complete_score = 
						&poker_complete_score_staight_flush;
					done = TRUE;
				}
				else
				{
					poker_free_stats_conn_suited( hcsStats );
				}
			}
		}
		
		if( hStats->num_equal_groups > 0 )
		{
			// There are some pairs etc. present: is there 4OAK or FH?
			struct _PokerGroup* grp;
			struct _PokerGroup* last_grp = hStats->equal_groups
					+ hStats->num_equal_groups - 1;
			
			for( grp = hStats->equal_groups; grp <= last_grp; ++grp )
			{
				char grp_number = grp->number;
				if( grp->group_size == 4 )
				{
					++score_info->num_fours;
					if( grp_number > score_info->best_4 )
					{
						score_info->best_4 = grp_number;
					}
				}
				else if( grp->group_size == 3 )
				{
					++score_info->num_threes;
					if( grp_number > score_info->best_3 )
					{
						score_info->second_best_3 = score_info->best_3;
						score_info->best_3 = grp_number;
					}
					else if( grp_number > score_info->second_best_3 )
					  {
						score_info->second_best_3 = grp_number;
					}
				}
				else if( grp->group_size == 2 )
				{
					++score_info->num_twos;
					if( grp_number > score_info->best_2 )
					{
						score_info->second_best_2 = score_info->best_2;
						score_info->best_2 = grp_number;
					}
					else if( grp_number > score_info->second_best_2 )
					{
						score_info->second_best_2 = grp_number;
					}
				}
			}
		
			if( score_info->num_fours > 0 )
			{   // Four of a kind
				done = TRUE;
				ret_score->hand_type = four_of_a_kind;
				
				*function_complete_score =  
					&poker_complete_score_four_of_a_kind;
			}
			else if( ( score_info->num_threes > 0
				&& score_info->num_twos > 0 ) )
			{   // Full house
				done = TRUE;
			
				ret_score->hand_type = full_house;
				
				*function_complete_score =  
					&poker_complete_score_full_house;
			}
		}
		
		if( !done )
		{
			if( !done_sStats )
			{
				done_sStats = TRUE;
				poker_copy_hand( &sHand, hHand );
				poker_convert_hand_num2suit( &sHand );
				poker_split_hand_suit( &sHand, split_sHand );
				_poker_analyse_suit( split_sHand, sStats );
			}
			
			if( sStats->num_suited > 0 )
			{	// Flush
				ret_score->hand_type = flush;
				
				*function_complete_score =  
					&poker_complete_score_flush;
			}
			else if( hStats->num_connecting > 0 )
			{   // Straight
				ret_score->hand_type = straight;
				
				*function_complete_score =  
					&poker_complete_score_straight_ace_high;
			}
			else if( done_lStats && lStats->num_connecting > 0 )
			{	// Straight with ace low
				ret_score->hand_type = straight;
				
				*function_complete_score =  
					&poker_complete_score_straight_ace_low;
			}
			else if( score_info->num_threes > 0 )
			{
				ret_score->hand_type = three_of_a_kind;
				
				*function_complete_score =  
					&poker_complete_score_three_of_a_kind;
			}
			else if( score_info->num_twos > 1 )
			{
				ret_score->hand_type = pair_of_pairs;
				
				*function_complete_score =  
					&poker_complete_score_pair_of_pairs;
			}
			else if( score_info->num_twos > 0 )
			{
				ret_score->hand_type = pair;
				
				*function_complete_score =  
					&poker_complete_score_pair;
			}
			else
			{
				ret_score->hand_type = high_card;
				
				*function_complete_score =  
					&poker_complete_score_high_card;
			}
		}
	}
	
	if( done_sStats )
	{
		poker_free_hand( &sHand );
	}
	
	if( done_lStats )
	{
		poker_free_hand( &lHand );
	}
}

void poker_complete_score_staight_flush(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info )
{
	ret_score->card1 = 1 + _poker_find_best_group_num(
		hcsStats->connecting_suited_groups,
		hcsStats->num_connecting_suited );
	ret_score->card2 = 255;
	ret_score->card3 = 255;
	ret_score->card4 = 255;
	ret_score->card5 = 255;
}

void poker_complete_score_four_of_a_kind(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info )
{
	ret_score->card1 = 1 + score_info->best_4;
	
	char values_to_ignore[1];
	
	values_to_ignore[0] = score_info->best_4;
	
	ret_score->card2 = 1 + _poker_find_next_best_card(
		split_hHand, values_to_ignore, 1 );
	
	ret_score->card3 = 255;
	ret_score->card4 = 255;
	ret_score->card5 = 255;
}

void poker_complete_score_full_house(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info )
{
	const char best_3 = score_info->best_3;
	const char second_best_3 = score_info->second_best_3;
	const char best_2 = score_info->best_2;
	
	ret_score->card1 = 1 + best_3;
	
	if( second_best_3 > best_2 )
	{
		ret_score->card2 = 1 + second_best_3;
	}
	else
	{
		ret_score->card2 = 1 + best_2;
	}
	ret_score->card3 = 255;
	ret_score->card4 = 255;
	ret_score->card5 = 255;
}

void poker_complete_score_flush(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info )
{
	char sorted_numbers[5];
	char* num;
	char* last_num;
	char* sorted_num;
	char* last_sorted_num;
	struct _PokerFlushGroup* best_group;

	_poker_find_best_group_suit( sStats->suited_groups,
		sStats->num_suited, &best_group );
	
	memset( sorted_numbers, 0, 5 /* *sizeof( char )*/ );
	
	// FUTURE: Simple insertion sorting for now.
	// Speed up if needed.
	last_sorted_num = sorted_numbers + 4;
	last_num = best_group->numbers + best_group->group_size - 1;
	for( num = best_group->numbers; num <= last_num; ++num )
	{
		for( sorted_num = sorted_numbers;
			 sorted_num <= last_sorted_num;
			 ++sorted_num )
		{
			if( *num > *sorted_num )
			{
				char* cp_num;
				for( cp_num = last_sorted_num - 1;
					 cp_num >= sorted_num;
					 --cp_num )
					{
					*(cp_num+1) = *cp_num;
				}
				*sorted_num = *num;
				break;
			}
		}
	}
	
	ret_score->card1 = 1 + sorted_numbers[0];
	ret_score->card2 = 1 + sorted_numbers[1];
	ret_score->card3 = 1 + sorted_numbers[2];
	ret_score->card4 = 1 + sorted_numbers[3];
	ret_score->card5 = 1 + sorted_numbers[4];
}

void poker_complete_score_straight_ace_high(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info )
{
	ret_score->card1 = 1 + _poker_find_best_group_num(
		hStats->connecting_groups, hStats->num_connecting );
	ret_score->card2 = 255;
	ret_score->card3 = 255;
	ret_score->card4 = 255;
	ret_score->card5 = 255;
}

void poker_complete_score_straight_ace_low(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info )
{
	ret_score->card1 = 1 + _poker_find_best_group_num(
		lStats->connecting_groups, lStats->num_connecting );
	ret_score->card2 = 255;
	ret_score->card3 = 255;
	ret_score->card4 = 255;
	ret_score->card5 = 255;
}

void poker_complete_score_three_of_a_kind(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info )
{
	char values_to_ignore[2];
	
	ret_score->card1 = 1 + score_info->best_3;
	
	values_to_ignore[0] = score_info->best_3;
	char best_next_card = _poker_find_next_best_card( split_hHand,
		values_to_ignore, 1 );
	
	ret_score->card2 = 1 + best_next_card;
	values_to_ignore[1] = best_next_card;
	
	ret_score->card3 = 1 + _poker_find_next_best_card( split_hHand,
		values_to_ignore, 2 );
	
	ret_score->card4 = 255;
	ret_score->card5 = 255;
}

void poker_complete_score_pair_of_pairs(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info )
{
	char values_to_ignore[2];
	ret_score->card1 = 1 + score_info->best_2;
	ret_score->card2 = 1 + score_info->second_best_2;
		
	values_to_ignore[0] = score_info->best_2;
	values_to_ignore[1] = score_info->second_best_2;
	
	ret_score->card3 = 1 + _poker_find_next_best_card(
		split_hHand, values_to_ignore, 2 );
	
	ret_score->card4 = 255;
	ret_score->card5 = 255;
}

void poker_complete_score_pair(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info )
{
	char values_to_ignore[3];
	char next_best;
	
	ret_score->card1 = 1 + score_info->best_2;
	
	values_to_ignore[0] = score_info->best_2;
	next_best = _poker_find_next_best_card( split_hHand,
		   values_to_ignore, 1 );
	ret_score->card2 = 1 + next_best;
	
	values_to_ignore[1] = next_best;
	next_best = _poker_find_next_best_card( split_hHand,
		values_to_ignore, 2 );
	ret_score->card3 = 1 + next_best;
	
	values_to_ignore[2] = next_best;
	next_best = _poker_find_next_best_card( split_hHand,
		values_to_ignore, 3 );
	ret_score->card4 = 1 + next_best;
	
	ret_score->card5 = 255;
}

void poker_complete_score_high_card(
	struct PokerHandScore* ret_score,
	struct PokerStatsNum* hStats,
	struct PokerStatsNum* lStats,
	struct PokerStatsSuit* sStats,
	struct PokerSplitHand* split_hHand,
	struct PokerSplitHand* split_lHand,
	struct PokerSplitHand* split_sHand,
	struct PokerStatsConnSuited* hcsStats,
	struct PokerScoreInfo* score_info )
{
	char values_to_ignore[4];
	char next_best;

	next_best = _poker_find_next_best_card( split_hHand,
		values_to_ignore, 0 );
	ret_score->card1 = 1 + next_best;
	
	values_to_ignore[0] = next_best;
	next_best = _poker_find_next_best_card( split_hHand,
		values_to_ignore, 1 );
	ret_score->card2 = 1 + next_best;
	
	values_to_ignore[1] = next_best;
	next_best = _poker_find_next_best_card( split_hHand,
		values_to_ignore, 2 );
	ret_score->card3 = 1 + next_best;
	
	values_to_ignore[2] = next_best;
	next_best = _poker_find_next_best_card( split_hHand,
		values_to_ignore, 3 );
	ret_score->card4 = 1 + next_best;
	
	values_to_ignore[3] = next_best;
	next_best = _poker_find_next_best_card( split_hHand,
		values_to_ignore, 4 );
	ret_score->card5 = 1 + next_best;
}

/* --------------------------------------------------------------- */

void _poker_analyse_numacehigh( struct PokerSplitHand* split_hHand,
	struct PokerStatsNum* hStats )
{
	struct PokerSplitCard* card;
	struct PokerSplitCard* last_card;
	int cur_equal_count = 0;
	int cur_equal_number = 0;
	int cur_connecting_count = 0;
	int cur_connecting_number = 0;
	
	hStats->num_equal_groups = 0;
	hStats->equal_groups = NULL;
	hStats->num_connecting = 0;
	hStats->connecting_groups = NULL;
	
	last_card = split_hHand->split_cards + split_hHand->num_cards - 1;
	for( card = split_hHand->split_cards; card <= last_card; ++card )
	{
		if( cur_equal_count == 0 )
		{   // If this is the first card, just set everything up
			cur_connecting_count = 1;
			cur_connecting_number = card->number;
			
			cur_equal_count = 1;
			cur_equal_number = card->number;
		}
		else if( card->number == cur_equal_number )
		{
			// This card is part of an equal group (potential pair/3/4)
			// Handle pair stuff here, but don't break the connecting counters,
			// since we may continue a straight after this card
			++cur_equal_count;
		}
		else
		{
			_poker_finished_equal_group( hStats,
				cur_equal_count, cur_equal_number );
			cur_equal_count = 1;
			cur_equal_number = card->number;
			
			if( card->number == cur_connecting_number + 1 )
			{
				// This card continues a connecting run (potential straight)
				++cur_connecting_count;
				++cur_connecting_number;
			}
			else
			{
				_poker_finished_connecting_group( hStats, cur_connecting_count,
					cur_connecting_number );
				// Reset our connecting watcher
				cur_connecting_count = 1;
				cur_connecting_number = card->number;
			}
		}
	}
	_poker_finished_connecting_group( hStats, cur_connecting_count,
		cur_connecting_number );
	_poker_finished_equal_group( hStats,
				cur_equal_count, cur_equal_number );
}

void _poker_analyse_numacelow( struct PokerSplitHand* split_lHand,
	struct PokerStatsNum* lStats )
{
	struct PokerSplitCard* card;
	struct PokerSplitCard* last_card;
	int cur_connecting_count = 0;
	int cur_connecting_number = 0;
	
	lStats->num_equal_groups = 0;	// Unused, but should set up
	lStats->equal_groups = NULL;	// Unused, but should set up
	lStats->num_connecting = 0;
	lStats->connecting_groups = NULL;
	
	last_card = split_lHand->split_cards + split_lHand->num_cards - 1;
	for( card = split_lHand->split_cards; card <= last_card; ++card )
	{
		if( cur_connecting_count == 0 )
		{   // If this is the first card, just set everything up
			cur_connecting_count = 1;
			cur_connecting_number = card->number;
		}
		else if( card->number == cur_connecting_number + 1 )
		{
			// This card continues a connecting run (potential straight)
			++cur_connecting_count;
			++cur_connecting_number;
		}
		else
		{
			break;	// We are only interest in the first connecting group:
					// none of the others could possibly contain an ace,
					// so they will have been picked up in the acehigh analysis
					// already.
		}
	}
	_poker_finished_connecting_group( lStats, cur_connecting_count,
		cur_connecting_number );
}

void _poker_analyse_connecting_suited( struct PokerSplitHand* split_nHand,
	struct PokerStatsConnSuited* cStats )
{
	// TODO: copy the num_acehigh stuff into here and only use the connecting
	//       suited stuff, and take it out of there.  Then only call this
	//       when we already have a straight and a flush.
	struct PokerSplitCard* card;
	struct PokerSplitCard* last_card;
	int cur_connecting_suited_count = 0;
	char cur_connecting_suited_number = 0;
	char cur_connecting_suited_suit = 0;
	
	cStats->num_connecting_suited = 0;
	cStats->connecting_suited_groups = NULL;
	
	last_card = split_nHand->split_cards + split_nHand->num_cards - 1;
	for( card = split_nHand->split_cards; card <= last_card; ++card )
	{
		int next_number;
		
		if( cur_connecting_suited_count == 0 )
		{   // If this is the first card, just set everything up
			cur_connecting_suited_count = 1;
			cur_connecting_suited_number = card->number;
			cur_connecting_suited_suit = card->suit;
		}
		else if(
			( next_number =
				( card->number == cur_connecting_suited_number + 1 ) )
			&& card->suit == cur_connecting_suited_suit )
		{
			// This card continues a connecting suited run (potential
			// straight flush)
			++cur_connecting_suited_count;
			++cur_connecting_suited_number;
		}
		else
		{
			_poker_finished_connecting_suited_group( cStats,
				cur_connecting_suited_count, cur_connecting_suited_number );
			// Reset our connecting suited watcher
			cur_connecting_suited_count = 1;
			cur_connecting_suited_number = card->number;
			cur_connecting_suited_suit = card->suit;
		}
	}
	_poker_finished_connecting_suited_group( cStats,
		cur_connecting_suited_count, cur_connecting_suited_number );
}

void _poker_analyse_suit( struct PokerSplitHand* split_sHand,
	struct PokerStatsSuit* sStats )
{
	struct PokerSplitCard* card;
	struct PokerSplitCard* last_card;
	struct _PokerFlushGroup cur_group;

	cur_group.group_size = 0;
	memset( cur_group.numbers, 0, 5 /* *sizeof( char )*/ );	// FUTURE: slow line
	
	sStats->num_suited = 0;
	sStats->suited_groups = NULL;
	
	last_card = split_sHand->split_cards + split_sHand->num_cards - 1;
	for( card = split_sHand->split_cards; card <= last_card; ++card )
	{
		if( cur_group.group_size == 0 )
		{   // If this is the first card, just set everything up
			cur_group.numbers[cur_group.group_size] = card->number;
			++cur_group.group_size;
			cur_group.suit = card->suit;
		}
		else if( card->suit == cur_group.suit )
		{
			if( cur_group.group_size < 5 )
			{	// Haven't found 5 yet
				cur_group.numbers[cur_group.group_size] = card->number;
				++cur_group.group_size;
				
			}
			else
			{	// We've found 5 cards already, so we only add this card
				// if it's better than one we already had.
				// FUTURE: are the cards sorted?  If so, can't we do better?
				char* n;
				char* last_n = cur_group.numbers + cur_group.group_size - 1;
				for( n = cur_group.numbers; n <= last_n; ++n )
				{
					if( card->number > *n )
					{
						*n = card->number;
						break;
					}
				}
			}
		}
		else
		{
			_poker_finished_suited_group( sStats, &cur_group );
			cur_group.group_size = 1;
			cur_group.numbers[0] = card->number;
			cur_group.suit = card->suit;
		}
	}
	_poker_finished_suited_group( sStats, &cur_group );
}

void _poker_finished_connecting_suited_group(
	struct PokerStatsConnSuited* nStats,
	const int cur_connecting_suited_count,
	const char cur_connecting_suited_number )
{
	// The connecting suited group has ended.  If we found
	// a connecting suited group of size >=5, remember it
	if( cur_connecting_suited_count >= 5 )
	{
		++(nStats->num_connecting_suited);
		
		nStats->connecting_suited_groups = realloc(
			nStats->connecting_suited_groups,
			sizeof( struct _PokerGroup ) * nStats->num_connecting_suited );
		
		struct _PokerGroup* grp = nStats->connecting_suited_groups +
			nStats->num_connecting_suited - 1;
		
		grp->group_size = cur_connecting_suited_count;
		grp->number = cur_connecting_suited_number;
	}
}

void _poker_finished_connecting_group( struct PokerStatsNum* nStats,
	const int cur_connecting_count,
	const char cur_connecting_number )
{
	// The connecting group has ended.  If we found
	// a connecting group of size >=5, remember it
	if( cur_connecting_count >= 5 )
	{
		++(nStats->num_connecting);
		
		nStats->connecting_groups = realloc(
			nStats->connecting_groups,
			sizeof( struct _PokerGroup ) * nStats->num_connecting );
		
		struct _PokerGroup* grp = nStats->connecting_groups +
			nStats->num_connecting - 1;
		
		grp->group_size = cur_connecting_count;
		grp->number = cur_connecting_number;
	}
}

void _poker_finished_equal_group( struct PokerStatsNum* nStats,
	const int cur_equal_count,
	const char cur_equal_number )
{
	// The equal group has ended.  If we found
	// an equal suited group of size >=5, remember it
	if( cur_equal_count >= 2 )
	{
		++(nStats->num_equal_groups);
		
		nStats->equal_groups = realloc(
			nStats->equal_groups,
			sizeof( struct _PokerGroup ) * nStats->num_equal_groups );
		
		struct _PokerGroup* grp = nStats->equal_groups +
			nStats->num_equal_groups - 1;
		
		grp->group_size = cur_equal_count;
		grp->number = cur_equal_number;
	}
}

void _poker_finished_suited_group( struct PokerStatsSuit* sStats,
	struct _PokerFlushGroup* cur_group )
{
	// The suited group has ended.  If we found
	// a suited group of size >=5, remember it
	if( cur_group->group_size >= 5 )
	{
		++(sStats->num_suited);
		sStats->suited_groups = realloc(
			sStats->suited_groups,
			sizeof( struct _PokerFlushGroup ) * sStats->num_suited );

		struct _PokerFlushGroup* grp = sStats->suited_groups +
			sStats->num_suited - 1;
		
		memcpy( grp, cur_group, sizeof( struct _PokerFlushGroup ) );
	}
}

void poker_free_stats_num( struct PokerStatsNum* nStats )
{
	free( nStats->equal_groups );
	free( nStats->connecting_groups );
}

void poker_free_stats_suit( struct PokerStatsSuit* sStats )
{
	free( sStats->suited_groups );
}

void poker_free_stats_conn_suited( struct PokerStatsConnSuited* nStats )
{
	free( nStats->connecting_suited_groups );
}

char _poker_find_best_group_num( struct _PokerGroup* grp, int size )
{
	char best_number = 0;
	struct _PokerGroup* last_grp = grp + size - 1;
	
	for( ; grp <= last_grp; ++grp )
	{
		char this_number = grp->number;
		if( this_number > best_number )
		{
			best_number = this_number;
		}
	}
	return best_number;
}

void _poker_find_best_group_suit( struct _PokerFlushGroup* grp, int size,
	struct _PokerFlushGroup** ret_best_group )
{
	char best_number = 0;
	struct _PokerFlushGroup* last_grp = grp + size - 1;
	*ret_best_group = grp;
	
	for( ; grp <= last_grp; ++grp )
	{
		char this_number = 0;
		char* n;
		char* last_n = grp->numbers + grp->group_size - 1;
		for( n = grp->numbers; n <= last_n; ++n )
		{
			if( *n > this_number )
			{
				this_number = *n;
			}
		}
		
		if( this_number > best_number )
		{
			best_number = this_number;
			*ret_best_group = grp;
		}
	}
}

char _poker_find_next_best_card( struct PokerSplitHand* split_hHand,
	char* values_to_ignore, int num_ignores )
{
	// Note: makes use of the fact that the hand supplied is sorted ascending
	                                                              
	char num;
	char* ig_val;
	char* last_ig_val;
	struct PokerSplitCard* nFirstSplitCard = split_hHand->split_cards;
	
	struct PokerSplitCard* nSplitCard;	
	for( nSplitCard = nFirstSplitCard + split_hHand->num_cards - 1;
		 nSplitCard >= nFirstSplitCard ; --nSplitCard )
	{
		num = nSplitCard->number;
		
		// Check whether we are ignoring this value
		last_ig_val = values_to_ignore + num_ignores - 1;
		for( ig_val = values_to_ignore; ig_val <= last_ig_val; ++ig_val )
		{
			if( *ig_val == num )
			{
				break;
			}
		}
		
		if( ig_val > last_ig_val )
		{
			// We are not ignoring this value, so we remember it as the best
			// so far.
			return num;
		}
	}
	
	return 255;
}

/*char _poker_find_next_best_card( struct PokerSplitHand* split_hHand,
	char* values_to_ignore, int num_ignores )
{
	char best_next_card = 0;
				
	struct PokerSplitCard* nSplitCard;
	struct PokerSplitCard* nLastSplitCard = split_hHand->split_cards
		+ split_hHand->num_cards - 1;
		
	for( nSplitCard = split_hHand->split_cards;
		 nSplitCard <= nLastSplitCard; ++nSplitCard )
	{
		char num = nSplitCard->number;
		if( num > best_next_card )
		{
			// Check whether we are ignoring this value
			char* ig_val;
			char* last_ig_val = values_to_ignore + num_ignores - 1;
			int ignoring = FALSE;
			for( ig_val = values_to_ignore; ig_val <= last_ig_val; ++ig_val )
			{
				if( *ig_val == num )
				{
					ignoring = TRUE;
					break;
				}
			}
			
			if( !ignoring )
			{
				// We are not ignoring this value, so we remember it as the best
				// so far.
				best_next_card = num;
			}
		}
	}
	
	return best_next_card;
}*/

int poker_compare_scores( struct PokerHandScore* score1,
	struct PokerHandScore* score2 )
{
	int ans = score1->hand_type - score2->hand_type;
	if( ans == 0 )
	{
		ans = score1->card1 - score2->card1;
		if( ans == 0 )
		{
			ans = score1->card2 - score2->card2;
			if( ans == 0 )
			{
				ans = score1->card3 - score2->card3;
				if( ans == 0 )
				{
					ans = score1->card4 - score2->card4;
					if( ans == 0 )
					{
						ans = score1->card5 - score2->card5;
					}
				}
			}
		}
	}
	return ans;
}

